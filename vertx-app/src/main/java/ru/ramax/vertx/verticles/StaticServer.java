package ru.ramax.vertx.verticles;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Handler;
import io.vertx.core.Promise;
import io.vertx.core.net.NetClientOptions;
import io.vertx.core.net.ProxyOptions;
import io.vertx.core.net.ProxyType;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.redis.client.*;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import ru.ramax.vertx.service.ProfileService;

import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;

import static io.netty.handler.codec.http.HttpHeaderNames.CONTENT_TYPE;
import static io.netty.handler.codec.http.HttpHeaderValues.APPLICATION_JSON;

public class StaticServer extends AbstractVerticle {
    private int httpPort = 8080;
    private final Router router = Router.router(vertx);
    private final ProfileService profileService = new ProfileService();



    @Override
    public void start(Promise<Void> startFuture) {
//        router.get("/api/profile").handler(this::middleHandler).handler(this::getProfileHandler);
//        vertx.createHttpServer().requestHandler(router).listen(httpPort, ar -> {
//            if (ar.succeeded()) {
//                System.out.println("HTTP server running on port: " + httpPort);
//                startFuture.complete();
//            } else {
//                System.out.println("Could not start a HTTP server: " + ar.cause());
//                startFuture.fail(ar.cause());
//            }
//        });

        var options = new RedisOptions()
                .setType(RedisClientType.STANDALONE)
                .setEndpoints(List.of("redis://localhost:30001"));

        // String key = "dic:shop:1";
        String key = "loycard:111:S:transactions";
        var client = Redis.createClient(vertx, options)
                .connect()
                .onSuccess(conn -> {
                    RedisAPI redis = RedisAPI.api(conn);
                    redis.lrange(key, "0", "2")
                            .onSuccess(value -> {

                                value.forEach( l -> System.out.println(
                                        l.toBuffer()
                                        .toJsonObject()
                                        .mapTo(CheckTransaction.class)));
                            })
                            .onFailure(e -> System.out.println("ERROR: "+ e));
                });


    }


    @Getter
    @Setter
    @ToString
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class CheckTransaction {
        @JsonProperty("rtl_txn_id")
        private String rtlTxnId;
        @JsonProperty("type_code")
        private String typeCode;
        @JsonProperty("store_id")
        private String storeId;
        @JsonProperty("name_tc")
        private String nameTc;
        @JsonProperty("cash_register_num")
        private String cashRegisterNum;
        @JsonProperty("title")
        private String title;
        @JsonProperty("created")
        private String created;
        @JsonProperty("amount_promo")
        private Integer amountPromo;
        @JsonProperty("amount_regular")
        private Integer amountRegular;
    }

    private void getProfileHandler(RoutingContext context) {
        profileService.getAccountById("1").onSuccess(res ->
                context.response()
                        .putHeader(CONTENT_TYPE, APPLICATION_JSON)
                        .end(res.encode())
        ).onFailure(err -> context.response().setStatusCode(500).end(err.getMessage()));
    }


    private void middleHandler(RoutingContext context) {
        System.out.println("Middle handler");
        context.next();
        return;
    }


}
