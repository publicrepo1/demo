package ru.ramax.vertx.service.query;

public class QueryTemplate {
    public static final String QUERY_BY_ACC_ID = "{\n" +
            "\tclm5_accounts(acc_id: 1) {\n" +
            "\t\tacc_owner_cus_id\n" +
            "\t\tacc_audit_md\n" +
            "\t\tacc_audit_mu\n" +
            "\t\tacc_hh_creation_date\n" +
            "\t\tacc_status\n" +
            "\t\tacc_id\n" +
            "\t\tacc_modification_date\n" +
            "\t\tcip_changed_date\n" +
            "\t\tacc_enrolment_date\n" +
            "\t\tcustomers {\n" +
            "      cus_acc_id\n" +
            "\t\t\tcards {\n" +
            "\t\t\t\tidn_no\n" +
            "\t\t\t\tidn_id\n" +
            "\t\t\t\tidn_pan\n" +
            "\t\t\t}\n" +
            "\t\t}\n" +
            "\t}\n" +
            "}\n";
}
