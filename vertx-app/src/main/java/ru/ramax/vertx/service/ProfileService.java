package ru.ramax.vertx.service;

import io.vertx.core.Future;
import io.vertx.core.Promise;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.client.WebClient;
import ru.ramax.vertx.model.ProfileRequest;
import ru.ramax.vertx.service.query.QueryTemplate;

public class ProfileService {
    private int port = 38190;
    private String host = "10.77.15.77";
    private String token = "53a5f77d-9361-43e7-827d-de8db7763296";
    private final WebClient client = WebClient.create(Vertx.vertx());

    public Future<JsonObject> getAccountById(String accountId) {
        Promise<JsonObject> promise = Promise.promise();
        JsonObject reqObject = new JsonObject();
        reqObject.put("query", QueryTemplate.QUERY_BY_ACC_ID);
        client.post(port, host, "/graphql")
                .bearerTokenAuthentication(token)
                .ssl(false)
                .sendJsonObject(reqObject, res -> {
                    if (res.succeeded()) {
                        promise.complete(res.result().bodyAsJsonObject());
                    } else {
                        promise.fail(res.cause());
                    }
                });

        return promise.future();
    }
}
